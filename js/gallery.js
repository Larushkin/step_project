'use strict'

let elem = document.querySelector('.grid');
let masonry = new Masonry( elem, {
    itemSelector: '.grid-item',
    columnWidth: 63,
    gutter: 1,
});
