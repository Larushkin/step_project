
document.querySelectorAll('.work_menu_items').forEach(el => {
    el.addEventListener('click', () => {
        document.querySelectorAll('.work_menu_items').forEach(el => el.classList.remove('items_active'));
        document.querySelectorAll('.gallery_card').forEach(el => el.classList.remove('card_active'));
        el.classList.add('items_active');
        document.querySelectorAll(`.gallery_card[data-work='${el.dataset.work}']`).forEach(el => el.classList.add('card_active'));

        if (el.dataset.work === 'all') {
            document.querySelectorAll(`.gallery_card[data-work='graphic_design']`).forEach(el => el.classList.add('card_active'))
            document.querySelectorAll(`.gallery_card[data-work='web_design']`).forEach(el => el.classList.add('card_active'))
            document.querySelector('.button_load').style = (display = 'block')
            document.querySelector('.button_load').classList.remove('loader')
        }
        if (el.dataset.work !== 'all') {
            document.querySelector('.work_button').style.display = 'none'
        } else {document.querySelector('.work_button').style = (display = 'block')}
    })

})

document.querySelector('.work_button').addEventListener('click', (event) => {
    event.target.style.display = 'none'

    document.querySelector('.button_load').classList.add('loader')
    let interval = setTimeout(function () {
        document.querySelector('.button_load').style.display = 'none'
        document.querySelectorAll(`.gallery_card[data-work='landing_pages']`).forEach(el => el.classList.add('card_active'))
        document.querySelectorAll(`.gallery_card[data-work='wordpress']`).forEach(el => el.classList.add('card_active'))
    }, 1500)
})